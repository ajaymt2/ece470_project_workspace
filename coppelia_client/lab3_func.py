#!/usr/bin/env python
import numpy as np
from scipy.linalg import expm

"""
Use 'expm' for matrix exponential.
Angles are in radian, distance are in meters.
"""

def Get_MS():
	# =================== Your code starts here ====================#
	# Fill in the correct values for S1~6, as well as the M matrix
	M = np.array([
		[1,0,0,0],
		[0,1,0,-367],
		[0,0,1,-692],
		[0,0,0,1]
	])
	q = np.array([
		[0, 0, 0],
		[0, -120, -152],
		[0, -120, -396],
		[0, -27, -609],
		[0, -110, -609],
		[0, -110, -692]
	])
	w = np.array([
		[0,0,-1],
		[0,-1,0],
		[0,-1,0],
		[0,-1,0],
		[0,0,-1],
		[0,-1,0]	
	])
	S = [np.append(wj, np.cross(wj,-qj)) for wj,qj in zip(w,q)]

	# ==============================================================#
	return M, S


def bracket(w):
	return np.array([
		[0,-w[2],w[1],w[3]],
		[w[2],0,-w[0],w[4]],
		[-w[1],w[0],0,w[5]],
		[0,0,0,0]
	])
def move(S,M,thetas):
	T=np.identity(4)
	for sa, theta in zip(S,thetas):
		T=np.matmul(T,expm(theta*bracket(sa)))
	return np.matmul(T,M)
"""
Function that calculates encoder numbers for each motor
"""
def lab_fk(theta1, theta2, theta3, theta4, theta5, theta6):

	# Initialize the return_value
	return_value = [None, None, None, None, None, None]

	# =========== Implement joint angle to encoder expressions here ===========
	print("Forward kinematics calculated:\n")
	M, S = Get_MS()
	thetas = [theta1, theta2, theta3, theta4, theta5, theta6]
	T=move(S,M,thetas)

	print(str(T) + "\n")

	return_value[0] = theta1# + np.pi
	return_value[1] = theta2
	return_value[2] = theta3
	return_value[3] = theta4# - (0.5*np.pi)
	return_value[4] = theta5
	return_value[5] = theta6

	return return_value
