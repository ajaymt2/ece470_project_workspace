import sim
import time
import numpy as np
from lab4_func import lab_invk
from lab3_func import lab_fk


print ('Program started')
sim.simxFinish(-1) # just in case, close all opened connections
clientID=sim.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to CoppeliaSim
if clientID == -1:
    print('Failed to connect to remote server')
    exit(1)


print ('Connected to remote API server')

sim.simxStartSimulation(clientID, sim.simx_opmode_oneshot)

print('Started simulation')

ur3_joint_handles = []
for i in range(1, 7):
    handle = sim.simxGetObjectHandle(clientID, "UR3_joint" + str(i), sim.simx_opmode_blocking)
    ur3_joint_handles.append(handle)

ur3_2_joint_handles = []
for i in range(1, 7):
    handle = sim.simxGetObjectHandle(clientID, "UR3_joint" + str(i) + "#0", sim.simx_opmode_blocking)
    ur3_2_joint_handles.append(handle)

return_code, hand_prox_sensor_handle = sim.simxGetObjectHandle(clientID, "Proximity_sensor0", sim.simx_opmode_blocking)
return_code, cuboid_2_handle = sim.simxGetObjectHandle(clientID, "Cuboid2", sim.simx_opmode_blocking)
sim.simxSetObjectIntParameter(clientID, hand_prox_sensor_handle, 4002, cuboid_2_handle, sim.simx_opmode_blocking)


def fold_and_place():
    sim.simxSetIntegerSignal(clientID, "conveyorBeltReset", 0, sim.simx_opmode_blocking)
    sim.simxSetIntegerSignal(clientID, "MicoClose", 0, sim.simx_opmode_blocking)
    sim.simxSetFloatSignal(clientID, "conveyorBeltVelocity", 0.16, sim.simx_opmode_blocking)

    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[0][1], np.radians(90), sim.simx_opmode_blocking)

    lab_fk(0, 0, np.radians(90), 0, 0, 0)
    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[2][1], np.radians(90), sim.simx_opmode_blocking)
    for i in range(6):
        if i == 2: continue
        sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[i][1], 0, sim.simx_opmode_blocking)

    return_code, prox_sensor_handle = sim.simxGetObjectHandle(clientID, "Proximity_sensor", sim.simx_opmode_blocking)
    data = sim.simxReadProximitySensor(clientID, prox_sensor_handle, sim.simx_opmode_streaming)
    print(data)

    while not data[1]:
        data = sim.simxReadProximitySensor(clientID, prox_sensor_handle, sim.simx_opmode_buffer)
        #print(data)

    print(data)

    sim.simxSetIntegerSignal(clientID, "MicoClose", 1, sim.simx_opmode_blocking)

    lab_fk(0, 0, np.radians(90), 0, 0, np.radians(-20))
    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[5][1], np.radians(-20), sim.simx_opmode_blocking)

    time.sleep(2)

    lab_fk(0, 0, 0, 0, 0, np.radians(-20))
    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[2][1], np.radians(0), sim.simx_opmode_blocking)
    sim.simxSetIntegerSignal(clientID, "MicoClose", 0, sim.simx_opmode_blocking)

    time.sleep(1)

    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[2][1], np.radians(-90), sim.simx_opmode_blocking)
    for i in range(6):
        if i == 2: continue
        sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[i][1], 0, sim.simx_opmode_blocking)

    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[5][1], np.radians(-20), sim.simx_opmode_blocking)
    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[0][1], np.radians(10), sim.simx_opmode_blocking)

    sim.simxSetFloatSignal(clientID, "conveyorBeltVelocity", -0.16, sim.simx_opmode_blocking)

    data = sim.simxReadProximitySensor(clientID, hand_prox_sensor_handle, sim.simx_opmode_streaming)
    while not data[1]:
        data = sim.simxReadProximitySensor(clientID, hand_prox_sensor_handle, sim.simx_opmode_buffer)

    sim.simxSetFloatSignal(clientID, "conveyorBeltVelocity", 0, sim.simx_opmode_blocking)

    sim.simxSetIntegerSignal(clientID, "MicoClose", 1, sim.simx_opmode_blocking)

    time.sleep(2)

    sim.simxSetJointTargetPosition(clientID, ur3_joint_handles[2][1], np.radians(0), sim.simx_opmode_blocking)
    sim.simxSetIntegerSignal(clientID, "MicoClose", 0, sim.simx_opmode_blocking)

    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[2][1], np.radians(-90), sim.simx_opmode_blocking)
    for i in range(6):
        if i == 2: continue
        sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[i][1], 0, sim.simx_opmode_blocking)

    time.sleep(1)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[0][1], np.radians(-90), sim.simx_opmode_blocking)
    time.sleep(1)
    sim.simxSetFloatSignal(clientID, "conveyorBeltVelocity2", -0.2, sim.simx_opmode_blocking)
    sim.simxSetIntegerSignal(clientID, "BaxterVacuumCup_active", 1, sim.simx_opmode_blocking)
    time.sleep(1)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[2][1], np.radians(-45), sim.simx_opmode_blocking)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[3][1], np.radians(-45), sim.simx_opmode_blocking)
    time.sleep(1)
    sim.simxSetFloatSignal(clientID, "conveyorBeltVelocity2", 0, sim.simx_opmode_blocking)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[0][1], np.radians(170), sim.simx_opmode_blocking)
    time.sleep(3)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[2][1], np.radians(-90), sim.simx_opmode_blocking)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[3][1], np.radians(0), sim.simx_opmode_blocking)
    time.sleep(0.5)
    sim.simxSetIntegerSignal(clientID, "BaxterVacuumCup_active", 0, sim.simx_opmode_blocking)
    sim.simxSetJointTargetPosition(clientID, ur3_2_joint_handles[2][1], np.radians(0), sim.simx_opmode_blocking)
    time.sleep(1)


while True:
    fold_and_place()
    sim.simxPauseSimulation(clientID, sim.simx_opmode_blocking)
    return_code, box = sim.simxGetObjectHandle(clientID, "Cuboid", sim.simx_opmode_blocking)
    return_code, new_obj_handles = sim.simxCopyPasteObjects(clientID, [box], sim.simx_opmode_blocking)
    joint_handle_1 = new_obj_handles[0] + 1
    joint_handle_2 = new_obj_handles[0] + 3
    sim.simxSetObjectPosition(clientID, new_obj_handles[0], -1, [-0.7, 0.55, 0.725], sim.simx_opmode_blocking)
    sim.simxSetObjectOrientation(clientID, new_obj_handles[0], -1, [0, 0, 0], sim.simx_opmode_blocking)
    sim.simxSetObjectOrientation(clientID, joint_handle_1, new_obj_handles[0], [np.radians(-90), 0, np.radians(100)], sim.simx_opmode_blocking)
    sim.simxSetObjectOrientation(clientID, joint_handle_2, new_obj_handles[0], [np.radians(-90), 0, np.radians(-100)], sim.simx_opmode_blocking)
    sim.simxSetObjectIntParameter(clientID, hand_prox_sensor_handle, 4002, new_obj_handles[0] + 4, sim.simx_opmode_blocking)
    sim.simxStartSimulation(clientID, sim.simx_opmode_blocking)


# Now close the connection to CoppeliaSim:
time.sleep(5)
#sim.simxStopSimulation(clientID, sim.simx_opmode_blocking)
sim.simxFinish(clientID)

