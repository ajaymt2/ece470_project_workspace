#!/usr/bin/env python
import numpy as np
from scipy.linalg import expm
# from lab4_header import *
import math
from lab3_func import lab_fk


"""
Function that calculates an elbow up Inverse Kinematic solution for the UR3
"""
def lab_invk(xWgrip, yWgrip, zWgrip, yaw_WgripDegree):
	# =================== Your code starts here ====================#

	L1 = .152
	L2 = .120
	L3 = .244
	L4 = .093
	L5 = .213
	L6 = .083
	L7 = .083
	L8 = .082
	L9 = .0535
	L10 = .059


	x_grip = xWgrip# + 0.15
	y_grip = yWgrip# - 0.15
	z_grip = zWgrip# - 0.01

	z_center = z_grip
	x_center = x_grip - (L9 * np.cos(math.radians(yaw_WgripDegree)))
	y_center = y_grip - (L9 * np.sin(math.radians(yaw_WgripDegree)))

	theta1 = np.arctan2(y_center, x_center) - math.asin((L2-L4+L6)/(math.sqrt(x_center**2 + y_center**2)))
	theta6 = math.pi/2 - (math.radians(yaw_WgripDegree) - theta1)

	x_3end = x_center - L7*np.cos(theta1) + (L6 + 0.027)*np.sin(theta1)							
	y_3end = y_center - L7 * np.sin(theta1) - (L6 + 0.027) * np.cos(theta1)
	z_3end = z_center + L10 + L8											

	hypotenuse = math.sqrt(x_3end**2 + y_3end**2+(z_3end - L1)**2)

	theta2 = np.arctan2((z_3end - L1), math.sqrt(x_3end**2 + y_3end**2)) + math.acos((L5**2-L3**2 - hypotenuse**2)/(-2*L3*hypotenuse))
	theta2 *= -1
	
	theta3 = math.pi - math.acos((hypotenuse**2-L3**2 - L5**2)/(-2*L3*L5))
	# temp1 = math.pi/2 - (math.pi - math.acos(L5**2-L3**2 - (hypotenuse**2)/(-2*L3*hypotenuse)) - math.acos((hypotenuse**2-L3**2 - L5**2)/(-2*L3*L5)))
	# temp2 = np.arctan2(math.sqrt(x_3end**2+y_3end**2),(z_3end-L1))
	# theta4 = (temp2 - temp1) * -1
	theta4 = -theta3 - theta2
	
	theta5 = (math.pi/2)*-1

	#return [math.degrees(theta1), math.degrees(theta2), math.degrees(theta3), math.degrees(theta4), math.degrees(theta5), math.degrees(theta6)]
	print([math.degrees(theta1), math.degrees(theta2), math.degrees(theta3), math.degrees(theta4), math.degrees(theta5), math.degrees(theta6)])
	return [theta1, -theta2, theta3 - np.pi/2, theta4, theta5, theta6]
	#return lab_fk(theta1, theta2, theta3, theta4, theta5, theta6)

	# ==============================================================#
	#pass