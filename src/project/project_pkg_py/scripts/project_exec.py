#!/usr/bin/env python

'''
We get inspirations of Tower of Hanoi algorithm from the website below.
This is also on the lab manual.
Source: https://www.cut-the-knot.org/recurrence/hanoi.shtml
'''

import os
import argparse
import copy
import time
import rospy
import rospkg
import numpy as np
import yaml
import sys
from math import pi
from project_header import *
import math

# 20Hz
SPIN_RATE = 20

# UR3 home location
home = np.radians([161.84,-68.36,89.56,-112.25,-90.51,74.17])

# Hanoi tower location 1
Q11 = np.radians([146.55,-42.39,91.91,-142.23,-89.56,57.33])
Q12 = np.radians([146.5,-48.87,91.98,-134.11,-90.22,57.33])
Q13 = np.radians([146.72,-52.46,87.38,-125.03,-90.5,56.89])

thetas = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

digital_in_0 = 0
analog_in_0 = 0

suction= True
current_suction = False
current_io_0 = False
current_position_set = False

# UR3 current position, using home position for initialization
current_position = copy.deepcopy(home)

############## Your Code Start Here ##############
"""
TODO: Initialize Q matrix
"""

Q21 = np.radians([163.79,-46.35,97.21,-140.06,-91.71,70.56])
Q22 = np.radians([163.58,-51.97,97.05,-134.22,-91.56,70.59])
Q23 = np.radians([163.58,-56.85,94.57,-126.81,-91.58,70.59])

Q31 = np.radians([177.69,-44.81,94.83,-140,-91.19,84.94])
Q32 = np.radians([177.68,-50.51,94.28,-133.23,-90.79,84.94])
Q33 = np.radians([177.84,-55.35,92.31,-126.35,-90.64,84.94])

Q = [ [Q11, Q12, Q13], \
      [Q21, Q22, Q23], \
      [Q31, Q32, Q33] ]
############### Your Code End Here ###############

############## Your Code Start Here ##############

"""
TODO: define a ROS topic callback funtion for getting the state of suction cup
Whenever ur3/gripper_input publishes info this callback function is called.
"""
def get_gripper_state(msg):
    global current_suction
    #suction = msg.
    current_suction = bool(msg.DIGIN)
############### Your Code End Here ###############


"""
Whenever ur3/position publishes info, this callback function is called.
"""
def position_callback(msg):

    global thetas
    global current_position
    global current_position_set

    thetas[0] = msg.position[0]
    thetas[1] = msg.position[1]
    thetas[2] = msg.position[2]
    thetas[3] = msg.position[3]
    thetas[4] = msg.position[4]
    thetas[5] = msg.position[5]

    current_position[0] = thetas[0]
    current_position[1] = thetas[1]
    current_position[2] = thetas[2]
    current_position[3] = thetas[3]
    current_position[4] = thetas[4]
    current_position[5] = thetas[5]

    current_position_set = True
    #rospy.loginfo("Pos: " + str(current_position))


def gripper(pub_cmd, loop_rate, io_0):

    global SPIN_RATE
    global thetas
    global current_io_0
    global current_position

    error = 0
    spin_count = 0
    at_goal = 0

    current_io_0 = io_0

    driver_msg = command()
    driver_msg.destination = current_position
    driver_msg.v = 1.0
    driver_msg.a = 1.0
    driver_msg.io_0 = io_0
    pub_cmd.publish(driver_msg)

    while(at_goal == 0):

        if( abs(thetas[0]-driver_msg.destination[0]) < 0.0005 and \
            abs(thetas[1]-driver_msg.destination[1]) < 0.0005 and \
            abs(thetas[2]-driver_msg.destination[2]) < 0.0005 and \
            abs(thetas[3]-driver_msg.destination[3]) < 0.0005 and \
            abs(thetas[4]-driver_msg.destination[4]) < 0.0005 and \
            abs(thetas[5]-driver_msg.destination[5]) < 0.0005 ):

            at_goal = 1

        loop_rate.sleep()

        if(spin_count >  SPIN_RATE*5):

            pub_cmd.publish(driver_msg)
            rospy.loginfo("Just published again driver_msg")
            spin_count = 0

        spin_count = spin_count + 1

    return error


def move_arm(pub_cmd, loop_rate, dest, vel = 4, accel = 4):

    global thetas
    global SPIN_RATE

    error = 0
    spin_count = 0
    at_goal = 0

    driver_msg = command()
    driver_msg.destination = dest
    driver_msg.v = vel
    driver_msg.a = accel
    driver_msg.io_0 = current_io_0
    pub_cmd.publish(driver_msg)

    loop_rate.sleep()

    while(at_goal == 0):

        if( abs(thetas[0]-driver_msg.destination[0]) < 0.0005 and \
            abs(thetas[1]-driver_msg.destination[1]) < 0.0005 and \
            abs(thetas[2]-driver_msg.destination[2]) < 0.0005 and \
            abs(thetas[3]-driver_msg.destination[3]) < 0.0005 and \
            abs(thetas[4]-driver_msg.destination[4]) < 0.0005 and \
            abs(thetas[5]-driver_msg.destination[5]) < 0.0005 ):

            at_goal = 1
            rospy.loginfo("Goal is reached!")

        loop_rate.sleep()

        if(spin_count >  SPIN_RATE*5):

            pub_cmd.publish(driver_msg)
            rospy.loginfo("Just published again driver_msg")
            spin_count = 0

        spin_count = spin_count + 1

    #thetas = dest
    return error


############## Your Code Start Here ##############

def move_block(pub_cmd, loop_rate, start_loc, start_height, \
               end_loc, end_height):
    global Q
    global home
    global current_suction

    ### Hint: Use the Q array to map out your towers by location and "height".

    error = None

    move_arm(pub_cmd,loop_rate,Q[start_loc][start_height])
    gripper(pub_cmd, loop_rate, 1)
    move_arm(pub_cmd, loop_rate, home)
    if not current_suction:
        return "Block not found!"

    move_arm(pub_cmd,loop_rate,Q[end_loc][end_height])
    gripper(pub_cmd, loop_rate, 0)
    move_arm(pub_cmd, loop_rate, home)

    return error


############### Your Code End Here ###############


def main():

    global home
    global Q
    global SPIN_RATE

    # Initialize ROS node
    rospy.init_node('lab2node')

    # Initialize publisher for ur3/command with buffer size of 10
    pub_command = rospy.Publisher('ur3/command', command, queue_size=10)

    # Initialize subscriber to ur3/position and callback fuction
    # each time data is published
    sub_position = rospy.Subscriber('ur3/position', position, position_callback)

    sub_gripper = rospy.Subscriber('ur3/gripper_input', gripper_input, get_gripper_state)

    ############## Your Code Start Here ##############
    # TODO: define a ROS subscriber for ur3/gripper_input message and corresponding callback function


    ############### Your Code End Here ###############


    ############## Your Code Start Here ##############
    # TODO: modify the code below so that program can get user input

    input_done = 0
    loop_count = 0

    start_tower = 0
    end_tower = 0

    while(not input_done):
        input_string = raw_input("Enter Start Tower and end Tower (Ex \"0 1\"): ")
        print("You entered " + input_string + "\n")

        try:
            start_tower, end_tower = int(input_string[0]), int(input_string[2])
            assert(start_tower!=end_tower)
            input_done = 1
        except:
            input_done = 0

    middle_tower = [i for i in range(0,3) if i not in  (start_tower,end_tower)][0]
    ############### Your Code End Here ###############

    # Check if ROS is ready for operation
    while(rospy.is_shutdown()):
        print("ROS is shutdown!")

    loop_rate = rospy.Rate(SPIN_RATE)

    ############## Your Code Start Here ##############
    # TODO: modify the code so that UR3 can move tower accordingly from user input

    Q = [Q[start_tower],Q[middle_tower],Q[end_tower]]

    move_arm(pub_command,loop_rate,home)
    start_locs = [0, 0, 2, 0, 1, 1, 0]
    start_heights = [2, 1, 0, 0, 1, 0, 0]
    end_locs = [2, 1, 1, 2, 0, 2, 2]
    end_heights = [0, 0, 1, 0, 0, 1, 2]
    for i in range(len(start_locs)):
        error = move_block(
                        pub_command, loop_rate, 
                        start_locs[i], start_heights[i], end_locs[i], end_heights[i]
                )
        if error != None:
            print(error)
            break

    gripper(pub_command, loop_rate, not suction)



    ############### Your Code End Here ###############


if __name__ == '__main__':

    try:
        main()
    # When Ctrl+C is executed, it catches the exception
    except rospy.ROSInterruptException:
        pass
