
desired position: (0.3160, 1.3080, 0.6748)
desired orientation: (0, 0, -90.0)

mass: 0.5kg
position: (0.3207, 1.3090, 0.6748)
orientation: (0, 0, -96.96)

mass: 1.0kg
position: (0.3219, 1.3080, 0.6748)
orientation: (0, 0, -92.59)

mass: 1.5kg
position: (0.3175, 1.3078, 0.6748)
orientation: (0, 0, -91.60)

mass: 2.0kg
position: (0.3165, 1.3080, 0.6748)
orientation: (0, 0, -90.91)

mass: 2.5kg
position: (0.3202, 1.3080, 0.6748)
orientation: (0, 0, -93.0)

mass: 3.0kg
position: (0.3313, 1.3074, 0.6748)
orientation: (0, 0, -95.83)

mass: 3.5kg
position: (0.3341, 1.3075, 0.6748)
orientation: (0, 0, -97.08)

mass: 4.0kg
position: (0.3359, 1.3071, 0.6748)
orientation: (0, 0, -97.92)

mass: 4.5kg
position: (0.3373, 1.3070, 0.6748)
orientation: (0, 0, -97.42)

mass: 5.0kg
position: (0.3547, 1.3053, 0.6748)
orientation (0, 0, -103.74)
