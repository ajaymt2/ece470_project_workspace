# Folding Bot

This repository contains the code and assets used to simulate the Folding Bot.

We began this project with Gazebo and then shifted to CoppeliaSim. Instructions for both simulators are below.

## CoppeliaSim
Run CoppeliaSim by navigating to its root (install/download) directory and running `coppeliaSim.sh`.
Open the `coppelia_test.ttt` scene (in the root directory of this repository), and run the following command in the interactive Lua command line:

```
simRemoteApi.start(19999)
```

Then, open another terminal and navigate to the `coppelia_client` directory in this repository and run

```
python3 fold.py
```

This should start the simulation and run the box folding code.

## Gazebo (Obsolete)
### Setup
Clone the repository and then run:
```
catkin_make
```

This should create a `build` directory.

### Run
To run the gazebo simulation, run the following commands in the workspace directory:
```
source devel/setup.bash
roslaunch ur3_driver ur3_gazebo.launch
```

This should launch Gazebo with the UR3 and the test world which contains three small cubes.

Open another terminal, navigate to the workspace directory and run:
```
source devel/setup.bash
rosrun project_pkg_py project_exec.py --simulator True
```

This will run the Hanoi Lab program in the simulator, utilizing digIn and interaction between elements.
