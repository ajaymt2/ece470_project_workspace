
import matplotlib.pyplot as plt

import math
from math import sqrt

target_position = (0.3160, 1.3080, 0.6748)
target_orientation = (0, 0, -90.0)

target_position

data = {
    0.5: ((0.3207, 1.3090, 0.6748), (0, 0, -96.96)),
    1.0: ((0.3219, 1.3080, 0.6748), (0, 0, -92.59)),
    1.5: ((0.3175, 1.3078, 0.6748), (0, 0, -91.60)),
    2.0: ((0.3165, 1.3080, 0.6748), (0, 0, -90.91)),
    2.5: ((0.3202, 1.3080, 0.6748), (0, 0, -93.0)),
    3.0: ((0.3313, 1.3074, 0.6748), (0, 0, -95.83)),
    3.5: ((0.3341, 1.3075, 0.6748), (0, 0, -97.08)),
    4.0: ((0.3359, 1.3071, 0.6748), (0, 0, -97.92)),
    4.5: ((0.3373, 1.3070, 0.6748), (0, 0, -97.42)),
    5.0: ((0.3547, 1.3053, 0.6748), (0, 0, -103.74))
}

errors = {}

for key, value in data.items():
    position, orientation = value
    pos_err = sqrt(sum([(v - target_position[i])**2 for i, v in enumerate(position)]))
    orientation_err = abs(orientation[2] - target_orientation[2])
    errors[key] = (pos_err, orientation_err)


masses = sorted(errors.keys())

f1 = plt.figure(1)
plt.plot(masses, [errors[m][0] for m in masses])
plt.ylabel("Distance to desired position [m]")
plt.xlabel("Mass of box [kg]")
f1.show()

f2 = plt.figure(2)
plt.plot(masses, [errors[m][1] for m in masses])
plt.ylabel("Error in orientation [degrees]")
plt.xlabel("Mass of box [kg]")
f2.show()

input()
